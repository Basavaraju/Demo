import {BookRepository} from "../../src/repositories/book.repository";
import {expect} from "chai";
import {suite, test} from 'mocha-typescript';
import {Book, BookStatus} from "../../src/models/Book";

@suite
export class BookRepositorySpec {
    private bookRepository: BookRepository;

    public before(): void {
        this.bookRepository = new BookRepository()
    }

    @test
    public shouldGetAllBooks(): void {
        //given
        const bookOne = new Book(1, 'Five Point Someone', 'RGV', '1998');
        const bookTwo = new Book(2, 'The Secret', 'P Goyal', '2001', BookStatus.CheckedOut);

        //when
        const allBooks = this.bookRepository.getAllBooks();

        //then
        expect(allBooks).to.be.lengthOf(3);
        expect(allBooks).to.deep.include(bookOne);
        expect(allBooks).to.deep.include(bookTwo);
    }

    @test
    public shouldGetBook(): void {
        const id: number = 1;
        const book = new Book(id, 'Five Point Someone', 'RGV', '1998');

        const expectedBook = this.bookRepository.getBookById(1);

        expect(book).to.deep.equal(expectedBook);
    }

    @test
    public shouldGetBookBasedOnProvidedAvailableStatus(): void {
        const bookOne = new Book(1, 'Five Point Someone', 'RGV', '1998', BookStatus.Available);
        const bookTwo = new Book(2, 'The Secret', 'P Goyal', '2001', BookStatus.CheckedOut);
        const bookThree = new Book(3, 'Success of failures', 'Raghu Ram', '1993', BookStatus.Available);

        //when
        const allBooks = this.bookRepository.getBooksOnStatus(BookStatus.Available);

        //then
        expect(allBooks).to.be.lengthOf(2);
        expect(allBooks).to.deep.include(bookOne);
        expect(allBooks).not.to.include(bookTwo);
        expect(allBooks).to.deep.include(bookThree);
    }

    @test
    public shouldUpdateBook(): void {
        //given
        const bookToUpdate = new Book(1, 'Five Point Someone', 'RGV', '1999', BookStatus.CheckedOut);

        //when
        const updatedBook = this.bookRepository.update(bookToUpdate);

        //then
        const allBooks = this.bookRepository.getAllBooks();
        expect(allBooks).to.include(bookToUpdate);
        expect(allBooks).lengthOf(3);
    }

    @test
    public shouldGetBookBasedOnProvidedCheckoutStatus(): void {
        const bookOne = new Book(1, 'Five Point Someone', 'RGV', '1999', BookStatus.Available);
        const bookTwo = new Book(2, 'The Secret','P Goyal','2001', BookStatus.CheckedOut);
        const bookThree = new Book(3, 'Success of failures','Raghu Ram','1993', BookStatus.Available);

        //when
        const allBooks = this.bookRepository.getBooksOnStatus(BookStatus.CheckedOut);

        //then
        expect(allBooks).to.be.lengthOf(2);
        expect(allBooks).not.to.deep.include(bookOne);
        expect(allBooks).to.deep.include(bookTwo);
        expect(allBooks).not.to.deep.include(bookThree);
    }
}