import {suite} from "mocha-typescript";
import {MovieRepository} from "../../src/repositories/movie.repository";
import {Movie, MovieStatus} from "../../src/models/Movie";
import {expect} from "chai";

@suite
export class MovieRepositorySpec {
    private movieRepository: MovieRepository;

    before() {
        this.movieRepository = new MovieRepository();
    }

    @test
    public shouldGetAvailableMovies() {
        const movieOne = new Movie(1, 'Test Movie1', 2000, 5);
        const movieTwo = new Movie(2, 'Test Movie2', 2001, 7, MovieStatus.Checkout);

       const actualMovies = this.movieRepository.getMoviesOnStatus(MovieStatus.Available);

        expect(actualMovies).to.deep.include(movieOne);
        expect(actualMovies).not.to.include(movieTwo);
    }
}