import {skip, suite, test} from 'mocha-typescript';
import {BookService} from "../../src/services/book.service";
import {BookRepository} from "../../src/repositories/book.repository";
import {Book, BookStatus} from "../../src/models/Book";
import {assert, expect} from 'chai';
import {capture, deepEqual, instance, mock, verify, when} from "ts-mockito";
import {BookNotFoundError} from "../../src/custom-errors/BookNotFoundError";
import {BookNotExistError} from "../../src/custom-errors/BookNotExistError";
import {InvalidBookStatusError} from "../../src/custom-errors/InvalidBookStatusError";

@suite
export class BookServiceSpec {
    private bookService: BookService;
    private bookRepository = mock(BookRepository);

    public before(): void {
        this.bookService = new BookService(instance(this.bookRepository));
    }

    @test
    public shouldGetAllBooks(): void {
        const bookOne = new Book(1, 'Test Book 1', 'Test Author1', '2000');
        const bookTwo = new Book(2, 'Test Book 2', 'Test Author2', '2012');
        when(this.bookRepository.getAllBooks()).thenReturn([bookOne, bookTwo]);

        const allBooks = this.bookService.getAllBooks();

        expect(allBooks).to.be.lengthOf(2);
        expect(allBooks[0]).to.equal(bookOne);
        expect(allBooks[1]).to.equal(bookTwo);
    }


    @test
    public shouldCheckOutBook(): void {
        const id: number = 1;
        const actualBook = new Book(id, 'Success of failures', 'Raghu Ram', '1993', BookStatus.Available);
        const expectedBook = new Book(id, 'Success of failures', 'Raghu Ram', '1993', BookStatus.CheckedOut);
        when(this.bookRepository.getBookById(id)).thenReturn(actualBook);

        this.bookService.checkoutBook(id);

        const [updateBookDetails] = capture(this.bookRepository.update).last();
        expect(updateBookDetails).to.deep.equal(expectedBook);
        verify(this.bookRepository.update(deepEqual(expectedBook))).once();
    }

    @test
    public shouldThrowBookNotFoundErrorIfBookIsNotAvailable(): void {
        const id: number = 1;
        const book = new Book(id, 'Success of failures', 'Raghu Ram', '1993', BookStatus.CheckedOut);
        when(this.bookRepository.getBookById(id)).thenReturn(book);
        assert.throws(() => this.bookService.checkoutBook(id), BookNotFoundError, 'Book is not available');
        verify(this.bookRepository.update(book)).never();
    }

    @test
    public shouldThrowBookNotExistErrorIfBookNotExist(): void {
        const id: number = 3;
        when(this.bookRepository.getBookById(id)).thenReturn(null);
        assert.throws(() => this.bookService.checkoutBook(id), BookNotExistError, "Book dose not exist");
        verify(this.bookRepository.update(null)).never();
    }

    @test
    public shouldGetAvailableBooks(): void {
        this.bookService.getBooksOnStatus(BookStatus.Available);

        verify(this.bookRepository.getBooksOnStatus(BookStatus.Available)).once();
    }

    @test
    public shouldThrowErrorWhenInvalidBookStatus(): void {
        assert.throws(() => this.bookService.getBooksOnStatus('ava'), InvalidBookStatusError, "Invalid Book Status Error");
    }

    @test
    public shouldReturnABook(): void {
        const bookId = 1;
        const oldBookStatus = new Book(bookId,'The First Five','RGV','1998',BookStatus.CheckedOut);
        const newBookStatus = new Book(bookId,'The First Five','RGV','1998',BookStatus.Available);
        when(this.bookRepository.getBookById(bookId)).thenReturn(oldBookStatus);
        when(this.bookRepository.update(deepEqual(newBookStatus))).thenReturn(newBookStatus);

        this.bookService.returnBook(bookId);

        verify(this.bookRepository.update(deepEqual(newBookStatus))).once();
        const [updateBookDetails] = capture(this.bookRepository.update).last();
        expect(updateBookDetails).to.deep.equal(newBookStatus);
    }

    @test
    public shouldThrowBookNotFoundError(): void {
        const bookId = 1;
        const oldBookStatus = new Book(bookId,'The First Five','RGV','1998',BookStatus.Available);
        when(this.bookRepository.getBookById(bookId)).thenReturn(oldBookStatus);

        assert.throws(() => this.bookService.returnBook(bookId), BookNotFoundError, `Book [${bookId}] is already Available, Please check the bookId.`);

    }
}