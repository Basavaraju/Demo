import {suite} from "mocha-typescript";
import {IdGenerator} from "../../src/utils/IdGenerator";
import {expect} from "chai";

@suite
export class IdGeneratorSpec {
    @test
    public shouldGenerateId() {

        //given
        const beforeTime = Date.now();
        const idGenerator = new IdGenerator();

        //when
        const actualId = idGenerator.generate();

        //then
        expect(actualId).to.gte(beforeTime);
        expect(actualId).to.lessThan(Date.now());

    }
}