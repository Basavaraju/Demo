import {suite} from "mocha-typescript";
import {BookController} from "../../src/controllers/book.controller";
import {BookService} from "../../src/services/book.service";
import {instance, mock, verify, when} from "ts-mockito";
import {assert, expect} from "chai";
import {Book} from "../../src/models/Book";

@suite
export class BookControllerSpec {
    private bookController: BookController;
    private bookService: BookService;

    before() {
        this.bookService = mock(BookService);
        this.bookController = new BookController(instance(this.bookService));
    }

    @test
    public shouldGetAllBooks(): void {
        const books = [new Book(1, 'The First Five', 'Chetan Bhagat', '2007'), new Book(2, 'The Secrets', 'Rhode Brayne', '2007')];
        when(this.bookService.getAllBooks()).thenReturn(books);

        const expectedBooks = this.bookController.getAllBooks();

        expect(expectedBooks).to.equal(books);
    }

    @test
    public shouldCheckoutGivenBook(): void {
        const bookId = 1;
        this.bookController.checkoutBook(bookId);
        verify(this.bookService.checkoutBook(bookId)).once();
    }

    @test
    public shouldReturnErrorIfBookIdIsUndefined(): void {
        const bookId = undefined;
        assert.throws(() => this.bookController.checkoutBook(bookId), Error, 'Invalid Input');
    }

    @test
    public shouldReturnErrorIfBookIdIsNegative(): void {
        const bookId = -1;
        assert.throws(() => this.bookController.checkoutBook(bookId), Error, 'Invalid Input');
    }

    @test
    public shouldGetAvailableBooks(): void {
        const status = 'available';
        this.bookController.getBooksOnStatus(status);

        verify(this.bookService.getBooksOnStatus(status)).once();
    }

    @test
    public shouldReturnInvalidErrorOnInvalidStatus(): void {
        const status = undefined;

        assert.throws(() => this.bookController.getBooksOnStatus(status), Error, 'Invalid input error');
    }

    @test
    public shouldReturnGivenBook(): void {
        const bookId = 1;
        this.bookController.return(bookId);
        verify(this.bookService.returnBook(bookId)).once();
    }

    @test
    public shouldThrowInvalidErrorOnInvalidStatusOnReturnBook(): void {
        const status = undefined;

        assert.throws(() => this.bookController.return(status), Error, 'Invalid Input');
    }


}