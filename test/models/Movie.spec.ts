import {suite} from "mocha-typescript";
import {expect} from 'chai';
import {Movie, MovieStatus} from "../../src/models/Movie";

@suite
export class MovieSpec {
    @test
    public shouldGetMovieStatus() {
        const movie1Status = MovieStatus.Available;
        const movie2Status = MovieStatus.Checkout;
        const movie1 = new Movie(1,'Test Movie1',2000,5,movie1Status);
        const movie2 = new Movie(2,'Test Movie2',2000,5,movie2Status);

        const firstMovieStatus = movie1.getStatus();
        const secondMovieStatus = movie2.getStatus();

        expect(firstMovieStatus).to.equal(movie1Status);
        expect(secondMovieStatus).to.equal(movie2Status);
    }

}