import {suite} from "mocha-typescript";
import {Book, BookStatus} from "../../src/models/Book";
import {assert, expect} from "chai";

@suite
export class BookSpec{
   @test
    public shouldChangeStatusOfBookToCheckout(){
       const availableBook = new BookSpecImp(1,'Success of failures','Raghu ram','1993');
       availableBook.checkout();
       expect(availableBook.status).to.equal(BookStatus.CheckedOut)
   }
}

export class BookSpecImp extends Book{
    public readonly _id: number;
    public readonly name: string;
    public status: BookStatus;
    public readonly author: string;
    public readonly publishedYear: string;

    constructor(id: number, name: string, author: string, publishedYear: string, status: BookStatus=BookStatus.Available) {
        super(id, name, author, publishedYear, status);
        this._id = id;
        this.name = name;
        this.status = status;
        this.author = author;
        this.publishedYear = publishedYear;
    }


    checkout(): void {
        super.checkout();
    }

    isBookAvailableForCheckout(): boolean {
        return super.isBookAvailableForCheckout();
    }

    getBookStatus(): BookStatus {
        return super.getBookStatus();
    }

    setAvailable(): void {
        super.setAvailable();
    }
}