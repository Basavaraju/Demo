export type RatingRange = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

export enum MovieStatus {
    Available = 'available',
    Checkout = 'checkout'
}

export class Movie {
    private readonly id: number;
    private readonly name: string;
    private readonly year: number;
    private readonly rating: RatingRange;
    private readonly status: MovieStatus;

    constructor(id: number, name: string, year: number, rating: RatingRange, status: MovieStatus = MovieStatus.Available) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.rating = rating;
        this.status = status;
    }

    getStatus(): MovieStatus {
        return this.status;
    }
}