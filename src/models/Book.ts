export enum BookStatus {
    Available = 'Available', CheckedOut = 'CheckedOut'
}

export class Book {
    public readonly _id: number;
    protected readonly name: string;
    protected status: BookStatus;
    protected readonly author: string;
    protected readonly publishedYear: string;

    constructor(id: number, name: string, author: string, publishedYear: string, status: BookStatus = BookStatus.Available) {
        this._id = id;
        this.name = name;
        this.status = status;
        this.author = author;
        this.publishedYear = publishedYear;
    }

    checkout() {
        this.status = BookStatus.CheckedOut;
    }

    isBookAvailableForCheckout(): boolean {
        return this.status !== BookStatus.Available
    }

    getBookStatus(): BookStatus {
        return this.status;
    }

    setAvailable() {
        this.status = BookStatus.Available;
    }
}