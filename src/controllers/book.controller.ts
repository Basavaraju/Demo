import {Book} from "../models/Book";
import {BookService} from "../services/book.service";

export class BookController {
    private bookService: BookService;

    constructor(bookService: BookService) {
        this.bookService = bookService;
    }

    getAllBooks(): Book[] {
        return this.bookService.getAllBooks();
    }

    checkoutBook(id: number) {
        if (!id || id < 0 || isNaN(id)) throw new Error('Invalid Input');
        this.bookService.checkoutBook(id);
    }

    public getBooksOnStatus(status: string): Book[] {
        if (status) {
            return this.bookService.getBooksOnStatus(status);
        }
        throw new Error('Invalid input error');
    }

    public return(id: number) {
        if (!id || id < 0 || isNaN(id)) throw new Error('Invalid Input');
        this.bookService.returnBook(id);
    }

}