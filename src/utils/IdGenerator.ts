export class IdGenerator {

    generate(): number {
        return Date.now();
    }
}