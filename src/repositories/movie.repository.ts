import {Movie, MovieStatus} from "../models/Movie";

export class MovieRepository {
    private static movieDB = [
        new Movie(1, 'Test Movie1', 2000, 5),
        new Movie(2, 'Test Movie2', 2001, 7, MovieStatus.Checkout)];


    getMoviesOnStatus(status: MovieStatus): Movie[] {
        return MovieRepository.movieDB.filter((movie) => movie.getStatus() === status);
    }
}