import {Book, BookStatus} from "../models/Book";

export class BookRepository {

    private static bookDB: Book[] = [new Book(1, 'Five Point Someone','RGV','1998',BookStatus.Available), new Book(2, 'The Secret','P Goyal','2001',BookStatus.CheckedOut),new Book(3, 'Success of failures','Raghu Ram','1993', BookStatus.Available)];

    public getAllBooks(): Book[] {
        return BookRepository.bookDB;
    }

    public update(book: Book): Book {
        const updatedBookId = book._id;
        const bookIndex = BookRepository.bookDB.findIndex((book) => book._id === updatedBookId);
        BookRepository.bookDB[bookIndex] = book;
        return BookRepository.bookDB[bookIndex];
    }

    public getBookById(id: number): Book {
        return BookRepository.bookDB.find((book) => {
            return book._id === id
        });
    }

    public getBooksOnStatus(status: BookStatus): Book[] {
        return BookRepository.bookDB.filter((book) => {
             return book.getBookStatus() === status;
        });
    }
}