export class BookNotExistError extends Error {
    constructor(message: string) {
        super(message);
    }
}