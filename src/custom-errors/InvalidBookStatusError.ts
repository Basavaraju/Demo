export class InvalidBookStatusError extends Error {
    constructor(message: string) {
        super(message);
    }
}