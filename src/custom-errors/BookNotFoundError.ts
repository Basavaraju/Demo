export class BookNotFoundError extends Error{
    constructor(message: string){
        super(message);
    }
}