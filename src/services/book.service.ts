import {Book, BookStatus} from "../models/Book";
import {BookRepository} from "../repositories/book.repository";
import {BookNotFoundError} from "../custom-errors/BookNotFoundError";
import {BookNotExistError} from "../custom-errors/BookNotExistError";
import {InvalidBookStatusError} from "../custom-errors/InvalidBookStatusError";

export class BookService {
    private bookRepository: BookRepository;

    constructor(bookRepository: BookRepository) {
        this.bookRepository = bookRepository;
    }

    public getAllBooks(): Book[] {
        return this.bookRepository.getAllBooks();
    }

    public checkoutBook(id: number): void {
        const book = this.getABook(id);
        if (book.isBookAvailableForCheckout()) throw new BookNotFoundError("Book is not available");
        book.checkout();
        this.bookRepository.update(book);
    }

    public getBooksOnStatus(status: string): Book[] {
        const bookStatus = this.getFilterStatus(status);
        return this.bookRepository.getBooksOnStatus(bookStatus);
    }

    private getABook(id: number) {
        const book = this.bookRepository.getBookById(id);
        if (!book) throw new BookNotExistError("Book dose not exist");
        return book;
    }

    private getFilterStatus(status: string) {
        if (BookStatus.Available.toUpperCase() == status.toUpperCase()) return BookStatus.Available;
        else if (BookStatus.CheckedOut.toUpperCase() == status.toUpperCase()) return BookStatus.CheckedOut;
        else throw new InvalidBookStatusError('Invalid Book Status Error');
    }

    public returnBook(bookId: number): void {
        const book = this.getABook(bookId);
        if (!book.isBookAvailableForCheckout()) throw new BookNotFoundError(`Book [${bookId}] is already Available, Please check the bookId.`)
        book.setAvailable();
        this.bookRepository.update(book);
    }
}