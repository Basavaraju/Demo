import express = require('express');
import {BookController} from "./controllers/book.controller";
import {BookService} from "./services/book.service";
import {BookRepository} from "./repositories/book.repository";
import {BookNotFoundError} from './custom-errors/BookNotFoundError';
import {BookNotExistError} from "./custom-errors/BookNotExistError";


const app = express();


app.get('/books', (req, res) => {
    try {
        if (!req.query.status) {
            const allBooks = new BookController(new BookService(new BookRepository())).getAllBooks();
            res.status(200).send(allBooks);
        } else {
            const status = req.query.status;
            console.log(`status for available [${status}]`);
            const availableBooks = new BookController(new BookService(new BookRepository())).getBooksOnStatus(status);
            res.send(availableBooks);
        }
    }catch (e) {
        if (e instanceof Error) return res.status(422).send(e.message);
    }
});

app.patch('/book/:bookId/checkout', (req, res) => {
    const bookId = parseInt(req.params.bookId);
    const bookController = new BookController(new BookService(new BookRepository()));
    try {
        bookController.checkoutBook(bookId);
    } catch (e) {
        if (e instanceof BookNotFoundError) return res.status(404).send(e.message);
    if (e instanceof BookNotExistError) return res.status(400).send(e.message);
    if (e instanceof Error) return res.status(422).send(e.message);
}
res.status(200).send(`Book [${bookId}] checked out successfully`);
});

app.patch('/book/:bookId/return', (req, res) => {
    const bookId = parseInt(req.params.bookId);
    const bookController = new BookController(new BookService(new BookRepository()));
    try {
        bookController.return(bookId);
    } catch (e) {
        if (e instanceof BookNotFoundError) return res.status(404).send(e.message);
        if (e instanceof BookNotExistError) return res.status(400).send(e.message);
        if (e instanceof Error) return res.status(422).send(e.message);
    }
    res.status(200).send(`Book [${bookId}] returned out successfully`);
});


app.listen(3000, () => console.log('Hey, I woke up'));
